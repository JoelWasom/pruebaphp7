<?php

define('__CONFIG__', [
    'db' => [
        'host' => 'mysql:host=localhost; dbname=prueba; charset=utf8',
        'user' => 'root',
        'password' => '',
    ],
    'log' => [
        'path' => 'log/',
        'channel' => 'prueba',
    ],
]);
