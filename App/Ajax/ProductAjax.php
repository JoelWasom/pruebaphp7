<?php
error_reporting(E_ALL);
set_error_handler(function ($severidad, $mensaje, $fichero, $línea) {
    if (!(error_reporting() & $severidad)) {
        return;
    }

    throw new \ErrorException($mensaje, 0, $severidad, $fichero, $línea);
});

require_once '../../vendor/autoload.php';
require_once '../../config.php';

use Prueba\Container;
use App\Models\Product;
use App\Services\ProductService;

Container::set('logger', function () {
    $logger = new \Monolog\Logger(__CONFIG__['log']['channel']);
    $file_handler = new \Monolog\Handler\StreamHandler(__CONFIG__['log']['path'] . date('Ymd') . '.log');
    $logger->pushHandler($file_handler);
    return $logger;
});

$logger = Container::get('logger');

/// post, get, y request
$accion = $_REQUEST['action'] ?? 0;
$nombre = $_POST['nombre'] ?? '';
$total = $_POST['total'] ?? 0;

$service = new ProductService;
$model = new Product;
$model->name = $nombre;
$model->total = $total;

switch($accion){
    case '0':
        echo "No se envio el parametro correcto.";
    break;
    case '1':
        echo ($service->getAll());
    break;
    case '2':
        $service->create($model);
    break;
    case '3' :
        echo ($service->getAllComboBox());
    break;
}