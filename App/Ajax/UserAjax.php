<?php
error_reporting(E_ALL);
set_error_handler(function ($severidad, $mensaje, $fichero, $línea) {
    if (!(error_reporting() & $severidad)) {
        return;
    }

    throw new \ErrorException($mensaje, 0, $severidad, $fichero, $línea);
});

require_once '../../vendor/autoload.php';
require_once '../../config.php';

use App\Models\User;
use Prueba\Container;
use App\Services\UserService;

Container::set('logger', function () {
    $logger = new \Monolog\Logger(__CONFIG__['log']['channel']);
    $file_handler = new \Monolog\Handler\StreamHandler(__CONFIG__['log']['path'] . date('Ymd') . '.log');
    $logger->pushHandler($file_handler);
    return $logger;
});

$logger = Container::get('logger');

/// post, get, y request
$accion = $_REQUEST['action'] ?? 0;
$nombre = $_POST['nombre'] ?? '';
$apellido = $_POST['apellido'] ?? '';
$documento = $_POST['documento'] ?? '';

$service = new UserService;
$model = new User;
$model->name = $nombre;
$model->last_name = $apellido;
$model->document = $documento;

switch($accion){
    case '0':
        echo '{"Success": false, "errors":{"reason": "No se envio el parametro correcto"}}';
    break;
    case '1':
        echo ($service->getAll());
    break;
    case '2':
        $service->create($model);
    break;
}