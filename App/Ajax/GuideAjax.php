<?php
error_reporting(E_ALL);
set_error_handler(function ($severidad, $mensaje, $fichero, $línea) {
    if (!(error_reporting() & $severidad)) {
        return;
    }

    throw new \ErrorException($mensaje, 0, $severidad, $fichero, $línea);
});

require_once '../../vendor/autoload.php';
require_once '../../config.php';

use Prueba\Container;
use App\Models\Guide;
use App\Services\GuideService;

Container::set('logger', function () {
    $logger = new \Monolog\Logger(__CONFIG__['log']['channel']);
    $file_handler = new \Monolog\Handler\StreamHandler(__CONFIG__['log']['path'] . date('Ymd') . '.log');
    $logger->pushHandler($file_handler);
    return $logger;
});

$logger = Container::get('logger');

/// post, get, y request
$accion = $_REQUEST['action'] ?? 0;
$producto_id = $_POST['producto'] ?? 0;
$descripcion = $_POST['descripcion'] ?? '';

$service = new GuideService;
$model = new Guide;
$model->product_id = $producto_id;
$model->description = $descripcion;

switch($accion){
    case '0':
        echo "No se envio el parametro correcto.";
    break;
    case '1':
        echo ($service->getAll());
    break;
    case '2':
        $service->create($model);
    break;
}