<?php
namespace App\Models;

class Guide
{
    public $id;
    public $guide_number;
    public $description;
    public $product_id;
    public $product;
}