<?php
namespace App\Services;

use PDOException;
use Prueba\Container;
use App\Models\Product;
use App\Repositories\ProductRepository;

class ProductService {
    private $_productRepository;
    private $_logger;
    
    public function __construct(){
        $this->_productRepository = new ProductRepository;
        $this->_logger = Container::get('logger');
    }

    public function getAll(){
        try {
            $result = $this->_productRepository->findAll();
        } catch (PDOException $ex) {
            $this->_logger->error($ex->getMessage());
        }
        return JSON_ENCODE($result);
    }
    public function getAllComboBox(){
        try {
            $result = $this->_productRepository->findAllCbx();
        } catch (PDOException $ex) {
            $this->_logger->error($ex->getMessage());
        }
        return JSON_ENCODE($result);
    }

    public function create(Product $model){
        try{
            $result = $this->_productRepository->add($model);
        }
        catch(PDOException $ex){
            $this->_logger->error($ex->getMessage());
        }
    }
}