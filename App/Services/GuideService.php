<?php
namespace App\Services;

use PDO;
use PDOException;
use App\Models\Guide;
use Prueba\Container;
use App\Repositories\GuideRepository;
use Prueba\Database\MySql\DbProvider;

class GuideService {
    private $_guideRepository;
    private $_logger;
    
    public function __construct(){
        $this->_db = DbProvider::get();
        $this->_guideRepository = new GuideRepository;
        $this->logger = Container::get('logger');
    }

    public function getAll(){
        try {
            $result = $this->_guideRepository->findAll();
        } catch (PDOException $ex) {
            $this->logger->error($ex->getMessage());
        }
        return JSON_ENCODE($result);
    }

    public function create(Guide $model): void{
        try{
            $this->logger->info('Comenzó la alta de registro [guia].');
            // Begin transacation
            $this->_db->beginTransaction();

            $result = $this->_guideRepository->add($model);

            // Commit transaction
            $this->_db->commit();
            $this->logger->info('Finalizó la alta de registro [guia]');
        }
        catch(PDOException $ex){
            $this->logger->error($ex->getMessage());
        }
    }
}