<?php
namespace App\Services;

use PDOException;
use App\Models\User;
use Prueba\Container;
use App\Repositories\UserRepository;

class UserService {
    private $_userRepository;
    private $_logger;
    
    public function __construct(){
        $this->_userRepository = new UserRepository;
        $this->logger = Container::get('logger');
    }

    public function getAll(){
        try {
            $result = $this->_userRepository->findAll();
        } catch (PDOException $ex) {
            $this->logger->error($ex->getMessage());
        }
        return JSON_ENCODE($result);
    }

    public function create(User $model): void{
        try{
            $result = $this->_userRepository->add($model);
        }
        catch(PDOException $ex){
            $this->logger->error($ex->getMessage());
        }
    }
}