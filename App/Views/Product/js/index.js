/*!
 * PROYECTO- JOEL
 * Copyright(c) 2020
 */

Ext.onReady(function () {
    Ext.namespace('Ext.dsdata');
    var indice = 'e';
    var confP = '';
    Ext.dsdata.storeProduct = new Ext.data.JsonStore({
        url: '../../Ajax/ProductAjax.php?action=1',
        root: 'data',
        totalProperty: 'total',
        fields: ['id', 'name', 'total']
    });

    var pagingProductoBar = new Ext.PagingToolbar({
        pageSize: 2000,
        store: Ext.dsdata.storeProduct,
        displayInfo: true,
        afterPageText: 'de {0}',
        beforePageText: 'Pag.',
        firstText: 'Primera Pag.',
        lastText: 'Ultima Pag.',
        nextText: 'Siguiente Pag.',
        prevText: 'Pag. Previa',
        refreshText: 'Refrescar',
        displayMsg: 'Desplegando del {0} - {1} de {2}',
        emptyMsg: "No hay elementos para desplegar."
    });
    var sm = new Ext.grid.CheckboxSelectionModel({
        listeners: {
            rowselect: function (sm, row, rec) {
                indice = row;
            }
        }
    });
    var ProductoColumnMode = new Ext.grid.ColumnModel(
        [
            sm,{
                header: 'ID',
                dataIndex: 'id',
                width: 50,
                align: 'center',
                sortable: true
            }, {
                header: 'NOMBRE',
                dataIndex: 'name',
                width: 150,
                sortable: true
            }, {
                header: 'TOTAL',
                dataIndex: 'total',
                width: 100,
                align: 'center',
                sortable: true
            }
        ]
    );
    var ProductoGrid = new Ext.grid.GridPanel({
        id: 'ProductoGrid',
        store: Ext.dsdata.storeProduct,
        region: 'center',
        cm: ProductoColumnMode,
        bbar: pagingProductoBar,
        enableColLock: false,
        stripeRows: true,
        selModel: new Ext.grid.RowSelectionModel({ singleSelect: true }),
        listeners:        {
            render: function () {
                Ext.dsdata.storeProduct.load();
            },
        },
        sm: sm,
    });

    var opc = 0;
    var PAmenu = new Ext.Panel({
        region: 'north',
        id: 'PAcabecera1',
        height: 29,
        tbar: [
            {
                id: 'nuev',
                text: '<a style ="color:#000000; font: bold 11px tahoma,arial,verdana,sans-serif;">Nuevo</a>',
                icon: '../../../img/Nuevo.png',
                handler: function (t) {
                    opc = 1;
                    add();
                }
            }
        ]
    });
    var viewport1 = new Ext.Viewport({
        layout: 'border',
        items: [PAmenu, ProductoGrid]
    });
});