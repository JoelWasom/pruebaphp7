/*!
 * PROYECTO- JOEL
 * Copyright(c) 2020
 */

Ext.onReady(function () {
    Ext.namespace('Ext.dsdata');
    var indice = 'e';
    var confP = '';
    Ext.dsdata.storeGuide = new Ext.data.JsonStore({
        url: '../../Ajax/GuideAjax.php?action=1',
        root: 'data',
        totalProperty: 'total',
        fields: ['id', 'guide_number', 'description', 'product_id', 'product']
    });

    var guidePaging = new Ext.PagingToolbar({
        pageSize: 2000,
        store: Ext.dsdata.storeGuide,
        displayInfo: true,
        afterPageText: 'de {0}',
        beforePageText: 'Pag.',
        firstText: 'Primera Pag.',
        lastText: 'Ultima Pag.',
        nextText: 'Siguiente Pag.',
        prevText: 'Pag. Previa',
        refreshText: 'Refrescar',
        displayMsg: 'Desplegando del {0} - {1} de {2}',
        emptyMsg: "No hay elementos para desplegar."
    });
    var sm = new Ext.grid.CheckboxSelectionModel({
        listeners: {
            rowselect: function (sm, row, rec) {
                indice = row;
            }
        }
    });
    var guideColumn = new Ext.grid.ColumnModel(
        [
            sm,{
                header: 'ID',
                dataIndex: 'id',
                width: 50,
                align: 'center',
                sortable: true
            }, {
                header: 'NRO GUIA',
                dataIndex: 'guide_number',
                width: 100,
                sortable: true
            }, {
                header: 'DESCRIPCION',
                dataIndex: 'description',
                width: 200,
                align: 'center',
                sortable: true
            }, {
                header: 'PRODUCTO',
                dataIndex: 'product',
                width: 150,
                sortable: true
            }
        ]
    );
    var guideGrid = new Ext.grid.GridPanel({
        id: 'guideGrid',
        store: Ext.dsdata.storeGuide,
        region: 'center',
        cm: guideColumn,
        bbar: guidePaging,
        enableColLock: false,
        stripeRows: true,
        selModel: new Ext.grid.RowSelectionModel({ singleSelect: true }),
        listeners:        {
            render: function () {
                Ext.dsdata.storeGuide.load();
            },
        },
        sm: sm,
    });

    var opc = 0;
    var PAmenu = new Ext.Panel({
        region: 'north',
        id: 'PAcabecera1',
        height: 29,
        tbar: [
            {
                id: 'nuev',
                text: '<a style ="color:#000000; font: bold 11px tahoma,arial,verdana,sans-serif;">Nuevo</a>',
                icon: '../../../img/Nuevo.png',
                handler: function (t) {
                    opc = 1;
                    add();
                }
            }
        ]
    });
    var viewport1 = new Ext.Viewport({
        layout: 'border',
        items: [PAmenu, guideGrid]
    });
});