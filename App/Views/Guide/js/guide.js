var winGuide;
var codigo;
var opcion;
Ext.namespace('Ext.dsdata');
Ext.dsdata.storeProduct = new Ext.data.JsonStore({
	url: '../../Ajax/ProductAjax.php?action=3',
	root: 'data',
	fields: ['id', 'name']
});
Ext.dsdata.storeProduct.load();

var cbProduct = new Ext.form.ComboBox({
	hiddenName: 'producto',
	width: 265,
	x: 100,
	y: 10,
	typeAhead: true,
	forceSelection: true,
	allowBlank: false,
	store: Ext.dsdata.storeProduct,
	emptyText: 'Seleccionar.',
	mode: 'local',
	forceSelection: true,
	triggerAction: 'all',
	selectOnFocus: true,
	editable: false,
	valueField: 'id',
	displayField: 'name',
	listeners: {
		'select': function (cmb, record, index) {
			txtDescripcion.focus(true, 300);
		}
	}
});
var txtDescripcion = new Ext.form.TextArea({
	name: 'descripcion',
	hideLabel: true,
	maxLength: 250,
	width: 265,
	x: 100,
	y: 40,
	allowBlank: false,
	style: { textTransform: "uppercase" },
	blankText: 'Campo requerido',
	emptyText: 'Máximo 255 caracteres.',
	enableKeyEvents: true,
	selectOnFocus: true,
	listeners: {
		keypress: function (t, e) {
			if (e.getKey() == 13) {
				btnAceptar_c.focus();
			}
		}
	}
});

// Labels
var lblcategoriap = new Ext.form.Label({
	text: 'Producto:',
	x: 10,
	y: 15,
	height: 70,
	cls: 'x-label'
});
var lblNombre = new Ext.form.Label({
	text: 'Descripcion:',
	x: 10,
	y: 45,
	height: 70,
	cls: 'x-label'
});


// botones
var btnAceptar_c = new Ext.Button({
	id: 'btnAceptar_c',
	icon: '../../../img/save.png',
	iconCls: 'x-btn-text-icon',
	text: '<a style ="color:GREEN; font: bold 11px tahoma,arial,verdana,sans-serif;">Aceptar</a>',
	style: { background: '#BCF5A9', borderRadius: '0px', border: '1px solid #cccccc' },
	minWidth: 80,
	handler: function () {
		frmGuide.guardarDatos();
	}
});
var btnLimpiar_c = new Ext.Button({
	id: 'btnLimpiar_c',
	icon: '../../../img/delete.png',
	iconCls: 'x-btn-text-icon',
	text: '<a style ="color:RED; font: bold 11px tahoma,arial,verdana,sans-serif;">Salir</a>',
	style: { background: '#F6CECE', borderRadius: '0px', border: '1px solid #cccccc' },
	minWidth: 80,
	handler: function () {
		IniComponet();
		winGuide.hide();
	}
});

var frmGuide = new Ext.FormPanel({
	frame: true,
	autoScroll: false,
	layout: 'absolute',
	items: [lblNombre, lblcategoriap, cbProduct, txtDescripcion],
	guardarDatos: function () {
		if (this.getForm().isValid()) {
			this.getForm().submit({
				url: '../../Ajax/GuideAjax.php?action=2',
				method: 'POST',
				waitTitle: 'Conectando',
				waitMsg: 'Enviando datos...',
				success: function (form, action) {
					var frm = frmGuide.getForm();
					frm.reset();
					frm.clearInvalid();
					winGuide.hide();
					Ext.dsdata.storeGuide.load();
				},
				failure: function (form, action) {
					if (action.failureType == 'server') {
						var data = Ext.util.JSON.decode(action.response.responseText);
						Ext.Msg.alert('No se pudo conectar', data.errors.reason, function () {
							txtDescripcion.focus(true, 1000);
						});
					}
					else {
						Ext.Msg.alert('Error!', 'Imposible conectar con servidor : ' + action.response.responseText);
					}
				}
			});
		}
	}
});
function IniComponet() {
	var frm = frmGuide.getForm();
	frm.reset();
	frm.clearInvalid();
}
function add() {
	if (!winGuide) {
		winGuide = new Ext.Window({
			layout: 'fit',
			width: 420,
			height: 200,
			title: 'Registrar Guia',
			resizable: false,
			closeAction: 'hide',
			closable: true,
			draggable: false,
			plain: true,
			border: false,
			modal: true,
			items: [frmGuide],
			buttonAlign:'center',
			buttons:[btnAceptar_c,'-','-', btnLimpiar_c],
			listeners: {
				show: function () {
					IniComponet();
					cbProduct.focus(true, 300);
				}
			}
		});
	}
	winGuide.show();
}
