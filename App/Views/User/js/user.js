
var winUser;

var txtNombre = new Ext.form.TextField({
	name: 'nombre',
	hideLabel: true,
	maxLength: 128,
	width: 265,
	x: 100,
	y: 15,
	allowBlank: false,
	style: { textTransform: "uppercase" },
	blankText: 'Campo requerido',
	enableKeyEvents: true,
	selectOnFocus: true,
	listeners: {
		keypress: function (t, e) {
			if (e.getKey() == 13) {
				txtApellido.focus();
			}
		}
	}
});
var txtApellido = new Ext.form.TextField({
	name: 'apellido',
	hideLabel: true,
	maxLength: 128,
	width: 265,
	x: 100,
	y: 45,
	allowBlank: false,
	style: { textTransform: "uppercase" },
	blankText: 'Campo requerido',
	enableKeyEvents: true,
	selectOnFocus: true,
	listeners: {
		keypress: function (t, e) {
			if (e.getKey() == 13) {
				txtDocumento.focus();
			}
		}
	}
});
var txtDocumento = new Ext.form.TextField({
	name: 'documento',
	hideLabel: true,	
	width: 265,
	x: 100,
	y: 75,
	allowBlank: true,
	blankText: 'Campo requerido',
	enableKeyEvents: true,
	selectOnFocus: true,
	listeners: {
		keypress: function (t, e) {
			if (e.getKey() == 13) {
				btnAceptar_ca.focus();
			}
		}
	}
});

// Labels
var lblNombre = new Ext.form.Label({
	text: 'Nombres:',
	x: 10,
	y: 20,
	height: 70,
	cls: 'x-label'
});
var lblApellido = new Ext.form.Label({
	text: 'Apellidos:',
	x: 10,
	y: 50,
	height: 70,
	cls: 'x-label'
});
var lblTotal = new Ext.form.Label({
	text: 'Documento:',
	x: 10,
	y: 80,
	height: 70,
	cls: 'x-label'
});

// botones
var btnAceptar_ca = new Ext.Button({
	id: 'btnAceptar_ca',
	text: '<a style ="color:GREEN; font: bold 11px tahoma,arial,verdana,sans-serif;">Aceptar</a>',
	style: { background: '#BCF5A9', borderRadius: '0px', border: '1px solid #cccccc' },
	minWidth: 80,
	handler: function () {
		frmUser.guardarDatos();
	}
});
var btnLimpiar_ca = new Ext.Button({
	id: 'btnLimpiar_ca',
	text: '<a style ="color:RED; font: bold 11px tahoma,arial,verdana,sans-serif;">Salir</a>',
	style: { background: '#F6CECE', borderRadius: '0px', border: '1px solid #cccccc' },
	minWidth: 80,
	handler: function () {
		var frm = frmUser.getForm();
		frm.reset();
		frm.clearInvalid();
		winUser.hide();
	}
});

var frmUser = new Ext.FormPanel({
	frame: true,
	layout: 'absolute',
	items: [ lblNombre, lblApellido, lblTotal,	txtNombre, txtApellido, txtDocumento ],
	guardarDatos: function () {
		if (this.getForm().isValid()) {
			this.getForm().submit({
				url: '../../Ajax/UserAjax.php?action=2',
				method: 'POST',
				waitTitle: 'Conectando',
				waitMsg: 'Enviando datos...',
				success: function (form, action) {
					var frm = frmUser.getForm();
					frm.reset();
					frm.clearInvalid();
					winUser.hide();
					Ext.dsdata.storeDocument.load();
				},
				failure: function (form, action) {
					if (action.failureType == 'server') {
						var data = Ext.util.JSON.decode(action.response.responseText);
						Ext.Msg.alert('No se pudo conectar', data.errors.reason, function () {
							txtNombre.focus(true, 100);
						});
					}
					else {
						Ext.Msg.alert('Error!', 'Imposible conectar con servidor : ' + action.response.responseText);
					}
				}
			});
		}
	}
});
function IniComponet() {
	var frm = frmUser.getForm();
	frm.reset();
	frm.clearInvalid();
}
function add() {
	if (!winUser) {
		winUser = new Ext.Window({
			layout: 'fit',
			width: 400,
			height: 210,
			title: 'Registrar Usuario.',
			resizable: false,
			closeAction: 'hide',
			closable: true,
			draggable: false,
			plain: true,
			border: false,
			modal: true,
			items: [frmUser],
			buttonAlign:'center',
			buttons:[btnAceptar_ca,'-','-', btnLimpiar_ca],
			listeners: {
				show: function () {
					IniComponet();
					txtNombre.focus(true, 300);
				}
			}
		});
	}
	winUser.show();
}
