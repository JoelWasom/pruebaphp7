/*!
 * PROYECTO- JOEL
 * Copyright(c) 2020
 */

Ext.onReady(function () {
    Ext.namespace('Ext.dsdata');
    var indice = 'e';
    var confP = '';
    Ext.dsdata.storeDocument = new Ext.data.JsonStore({
        url: '../../Ajax/UserAjax.php?action=1',
        root: 'data',
        totalProperty: 'total',
        fields: ['id', 'name', 'last_name', 'document']
    });

    var pagingBar = new Ext.PagingToolbar({
        pageSize: 2000,
        store: Ext.dsdata.storeDocument,
        displayInfo: true,
        afterPageText: 'de {0}',
        beforePageText: 'Pag.',
        firstText: 'Primera Pag.',
        lastText: 'Ultima Pag.',
        nextText: 'Siguiente Pag.',
        prevText: 'Pag. Previa',
        refreshText: 'Refrescar',
        displayMsg: 'Desplegando del {0} - {1} de {2}',
        emptyMsg: "No hay elementos para desplegar."
    });
    var sm = new Ext.grid.CheckboxSelectionModel({
        listeners: {
            rowselect: function (sm, row, rec) {
                indice = row;
            }
        }
    });
    var Columns = new Ext.grid.ColumnModel(
        [
            sm,{
                header: 'ID',
                dataIndex: 'id',
                width: 50,
                align: 'center',
                sortable: true
            }, {
                header: 'NOMBRES',
                dataIndex: 'name',
                width: 150,
                sortable: true
            }, {
                header: 'APELLIDOS',
                dataIndex: 'last_name',
                width: 150,
                sortable: true
            }, {
                header: 'DOCUMENTO',
                dataIndex: 'document',
                width: 100,
                align: 'center',
                sortable: true
            }
        ]
    );
    var UserGrid = new Ext.grid.GridPanel({
        id: 'UserGrid',
        store: Ext.dsdata.storeDocument,
        region: 'center',
        cm: Columns,
        bbar: pagingBar,
        enableColLock: false,
        stripeRows: true,
        selModel: new Ext.grid.RowSelectionModel({ singleSelect: true }),
        listeners:        {
            render: function () {
                Ext.dsdata.storeDocument.load();
            },
        },
        sm: sm,
    });

    var opc = 0;
    var PAmenu = new Ext.Panel({
        region: 'north',
        id: 'PAcabecera1',
        height: 29,
        tbar: [
            {
                id: 'nuev',
                text: '<a style ="color:#000000; font: bold 11px tahoma,arial,verdana,sans-serif;">Nuevo</a>',
                icon: '../../../img/Nuevo.png',
                handler: function (t) {
                    opc = 1;
                    add();
                }
            }
        ]
    });
    var viewport1 = new Ext.Viewport({
        layout: 'border',
        items: [PAmenu, UserGrid]
    });
});