<?php
namespace App\Repositories;

use PDO;
use App\Models\Product;
use Prueba\Database\MySql\DbProvider;

class ProductRepository
{
    private $_db;

    public function __construct()
    {
        $this->_db = DbProvider::get();
    }

    public function findAll(): array
    {
        $result = [];
        $sql = 'select * from products order by name asc';

        $stm = $this->_db->prepare($sql);
        $stm->execute();
        $result = $stm->fetchAll();

        $paging = array(
            "succes" => true,
            "data" => $result
        );
        return $paging;
    }
    public function findAllCbx(): array
    {
        $result = [];
        $sql = 'select id, name from products order by name asc';

        $stm = $this->_db->prepare($sql);
        $stm->execute();
        $result = $stm->fetchAll();

        $paging = array(
            "succes" => true,
            "data" => $result
        );
        return $paging;
    }

    public function add(Product $model): void
    {
        $sql = 'insert into products(name, total) values (:name, :total)';
        $stm = $this->_db->prepare($sql);
        $stm->execute([
            'name' => $model->name,
            'total' => $model->total
        ]);
    }
}
