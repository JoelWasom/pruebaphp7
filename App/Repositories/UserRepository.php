<?php
namespace App\Repositories;

use PDO;
use App\Models\User;
use Prueba\Database\MySql\DbProvider;

class UserRepository
{
    private $_db;

    public function __construct()
    {
        $this->_db = DbProvider::get();
    }

    public function findAll(): array
    {
        $result = [];
        $sql = 'select * from users order by name asc';

        $stm = $this->_db->prepare($sql);
        $stm->execute();
        $result = $stm->fetchAll();

        $paging = array(
            "succes" => true,
            "data" => $result
        );
        return $paging;
    }

    public function add(User $model): void
    {
        $result = $this->validar_documento($model->document);
        if($result == 0){
            $sql = 'insert into users(name, last_name, document) values (:name, :last_name, :document)';
            $stm = $this->_db->prepare($sql);
            $stm->execute([
                'name' => $model->name,
                'last_name' => $model->last_name,
                'document' => $model->document
            ]);
        } else {
            echo '{"Success": false, "errors":{"reason": "Documento Invalido."}}';
        }
    }

    private function validar_documento($documento){
        $sql_b = "select * from users where document = :document";
        $stm = $this->_db->prepare($sql_b);
        $stm ->execute(['document' =>$documento]);
        $data = $stm->fetchObject('\\App\\Models\\User');
        if($data){
            return 1;
        } else {
            return 0;
        }
    }
}
