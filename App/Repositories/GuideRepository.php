<?php
namespace App\Repositories;

use PDO;
use App\Models\Guide;
use Prueba\Database\MySql\DbProvider;

class GuideRepository
{
    private $_db;

    public function __construct()
    {
        $this->_db = DbProvider::get();
    }

    public function findAll(): array
    {
        $result = [];
        $sql = 'select g.*, p.name as product from guide g inner join products p on g.product_id = p.id order by guide_number';

        $stm = $this->_db->prepare($sql);
        $stm->execute();
        $result = $stm->fetchAll();

        $paging = array(
            "succes" => true,
            "data" => $result
        );
        return $paging;
    }


    public function add(Guide $model): void
    {
        $sql = 'insert into guide(guide_number, description, product_id) values (:guide_number, :description, :product_id)';
        $stm = $this->_db->prepare($sql);
        $stm->execute([
            'guide_number' => $this->guide_number(),
            'description' => $model->description,
            'product_id' => $model->product_id
        ]);
    }

    private function guide_number(){
        $desdeLetra = "A";
        $hastaLetra = "Z";
        $desdeNumero = 1;
        $hastaNumero = 9999;
        $letraAleatoria = chr(rand(ord($desdeLetra), ord($hastaLetra)));
        $numeroAleatorio = rand($desdeNumero, $hastaNumero);

        $numero = str_pad($numeroAleatorio, 4, '0', STR_PAD_LEFT);
        $numero_guia = "$letraAleatoria$numero";

        $sql = "select guide_number from guide where guide_number = :nro";
        $stm = $this->_db->prepare($sql);
        $stm->execute(['nro' => $numero_guia]);
        $data = $stm->fetchObject('\\App\\Models\\Guide');
        if($data){
            guide_number();
        }else{
            $result = $numero_guia;
        }
        return $result;
    }
}
