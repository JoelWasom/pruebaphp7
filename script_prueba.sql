-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.3.16-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para prueba
CREATE DATABASE IF NOT EXISTS `prueba` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `prueba`;

-- Volcando estructura para tabla prueba.guide
CREATE TABLE IF NOT EXISTS `guide` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `guide_number` varchar(5) NOT NULL,
  `description` varchar(255) NOT NULL,
  `product_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `guide_number` (`guide_number`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `guide_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prueba.guide: ~3 rows (aproximadamente)
DELETE FROM `guide`;
/*!40000 ALTER TABLE `guide` DISABLE KEYS */;
INSERT INTO `guide` (`id`, `guide_number`, `description`, `product_id`) VALUES
	(1, 'G7692', 'prueba 1', 5),
	(2, 'B3036', 'test', 3),
	(3, 'F1144', 'test', 5);
/*!40000 ALTER TABLE `guide` ENABLE KEYS */;

-- Volcando estructura para tabla prueba.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `total` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prueba.products: ~8 rows (aproximadamente)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `total`) VALUES
	(1, 'producto 2', '20'),
	(2, 'producto 1', '10'),
	(3, 'producto 5', '50'),
	(4, 'producto 3', '45'),
	(5, 'producto 4', '5'),
	(6, 'producto 7', '8'),
	(7, 'producto 8', '51'),
	(8, 'producto 6', '7');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla prueba.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `document` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document` (`document`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla prueba.users: ~6 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `last_name`, `document`) VALUES
	(1, 'joel', 'barco', '9061336'),
	(3, 'jafet', 'barco', '86516'),
	(4, 'test', 'test', '85664'),
	(5, 'test', 'teee', '7654'),
	(6, 'maria', 'maria', '3333'),
	(7, 'luis', 'prado', '26255');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
